# Adatree OIDC Sample application

## Quickstart

```
yarn install
```

```
yarn start
```

<br/>
<br/>

# Simulate Backend API and Authentication

By default, the app runs with a simulated Backend API and Authentication.

## Prerequisite

Before turning off the simulated Backend API and Authentication to use the Adatree Consent APIs, you will need an account created with a username and password. Please contact the Adatree to provide you with these details.

<br/>

## Simulate Backend API

To turn the simulated Backend API off, open `env.sample` and update `REACT_APP_SIMULATE_BACKEND` to `false`.

```
REACT_APP_SIMULATE_BACKEND='false'
```

See Adatree API Configuration below for more details.

<br/>

## Simulate Authentication

To turn the simulated Authentication off, open `env.sample` and update `REACT_APP_SIMULATE_AUTHENTICATION` to `false`.

```
REACT_APP_SIMULATE_AUTHENTICATION='false'
```

See OIDC Authentication Configuration below for more details.

<br/>
<br/>

# Configuration

Open `env.sample` to configure the Adatree API and OIDC authentication.

## Adatree API Configuration

Please contact the team at Adatree to get the values for your API.

| Name                                  | Example value                    | Description                                                                                                               |
| ------------------------------------- | -------------------------------- | ------------------------------------------------------------------------------------------------------------------------- |
| REACT_APP_ADH_REDIRECT_URI            | http://YOUR_DOMAIN/redirect      | The redirect URI of your client application to receive the response from the Accredited Data Holder                       |
| REACT_APP_BACKEND_PROTOCOL            | https                            |                                                                                                                           |
| REACT_APP_BACKEND_DOMAIN              | example.api.adatree.com.au       | Contact the team at Adatree to get the values for your API                                                                |
| REACT_APP_CONSENT_USE_CASE            | HOME_LOAN                        | Use Case ID                                                                                                               |
| REACT_APP_DEFAULT_SOFTWARE_PRODUCT_ID | AB123456-1234-1234-1234-AB123456 | The identifier of the software product registered at the ACCC registry to be associated with this authentication request. |

<br/>

## OIDC Authentication Configuration

| Name                                | Example value                                          | description                                                                                          |
| ----------------------------------- | ------------------------------------------------------ | ---------------------------------------------------------------------------------------------------- |
| REACT_APP_OIDC_AUTHORITY_URI        | https://YOUR_AUTH_PROVIDER/authorize                   | The URL of the OIDC/OAuth2 provider                                                                  |
| REACT_APP_OIDC_CLIENT_ID            | YOUR_OIDC_CLIENT_ID                                    | Your client application's identifier as registered with the OIDC/OAuth2                              |
| REACT_APP_OIDC_END_SESSION_ENDPOINT | https://YOUR_AUTH_PROVIDER/logout?client_id=&returnTo= | The end session endpoint from the OIDC/OAuth2 provider                                               |
| REACT_APP_OIDC_EXTRA_QUERY_PARAMS   | {"audience":"https://YOUR_AUTH_PROVIDER/"}             | An object containing additional query string parameters to be including in the authorization request |
| REACT_APP_OIDC_ISSUER               | https://YOUR_AUTH_PROVIDER/                            | The OIDC/OAuth2 provider URI                                                                         |
| REACT_APP_OIDC_JWKS_URI             | https://YOUR_AUTH_PROVIDER/.well-known/jwks.json       | JSON Web Key Set endpoint from the OIDC/OAuth2 provider                                              |
| REACT_APP_OIDC_REDIRECT_URI         | http://YOUR_DOMAIN/authentication                      | The redirect URI of your client application to receive a response from the OIDC/OAuth2 provider      |
| REACT_APP_OIDC_RESPONSE_TYPE        | id_token                                               | The type of response desired from the OIDC/OAuth2 provider                                           |
| REACT_APP_OIDC_SCOPE                | OpenID email profile consumer:consents:read            | The scope being requested from the OIDC/OAuth2 provider                                              |
| REACT_APP_OIDC_TOKEN_ENDPOINT       | https://YOUR_AUTH_PROVIDER/oauth/token                 | The token endpoint from the OIDC/OAuth2 provider                                                     |
| REACT_APP_OIDC_USERINFO_ENDPOINT    | https://YOUR_AUTH_PROVIDER/userinfo                    | The user info endpoint from the OIDC/OAuth2 provider                                                 |
|                                     |

<br/>
<br/>

# About the App

- This app is a SPA using React CRA and MUI 5
- This app uses Adatree's component library from the @adatree-oss NPM package

<br/>

## @adatree-oss registry

The registry URL is set in `.yarnrc`.
