import { ThemeProvider } from '@adatree-oss/atomic-components';
import { Router } from 'react-router-dom';
import { Routes } from './routes/routes';
import { createBrowserHistory } from 'history';
import { Header } from '../components/organisms/navigation/header.organism';
import { themeOverride } from './theme/theme-override';
import { AuthenticationProvider } from '../providers/authentication.provider';
import { bootstrapApp } from '../components/organisms/bootstrap/bootstrap.organism';

export const history = createBrowserHistory();

function App() {
  bootstrapApp();

  const extendTheme = themeOverride();

  return (
    <>
      <Router history={history}>
        <AuthenticationProvider>
          <ThemeProvider extendTheme={extendTheme}>
            <Header />
            <Routes />
          </ThemeProvider>
        </AuthenticationProvider>
      </Router>
    </>
  );
}

export default App;
