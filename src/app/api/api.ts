import { Logger } from '@adatree-oss/atomic-components';
import { signout } from '../authentication/authentication';
import axios from 'axios';

export const setApiInterceptors = (): void => {
  axios.interceptors.response.use(
    (res) => {
      return res;
    },
    (err) => {
      if (err.response && err.response.status) {
        switch (err.response.status) {
          case 401:
            Logger.warn(
              'Server responded with a 401, logging the user out of their Auth Provider since their access token is unauthorized.'
            );
            signout();
            break;
          default:
            break;
        }
      }

      return Promise.reject(err);
    }
  );
};
