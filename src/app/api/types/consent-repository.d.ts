interface ConsentRepository {
  findAllConsents(): Promise<ConsentResponse[]>;
  findAllDataHolders(): Promise<DataHolder[]>;
  findAllUseCases(): Promise<DataHolder[]>;
  findConsentById(consentId: string): Promise<ConsentResponse>;
  createConsent(consent: CreateConsent): Promise<ConsentResponse>;
  updateConsent(consentId: string, consent: UpdateConsent): Promise<ConsentResponse>;
  revokeConsent(consentId: string): Promise<ConsentResponse>;
  authorization(dataHolderBrandId: string, consentId: string, cdrArrangementId?: string): Promise<string>;
  processAuthorization(state: string, code: string, idToken: string): Promise<string>;
}

export default ConsentRepository;
