import { TestUtil } from '@adatree-oss/atomic-components';
import { initState } from '../state/state';
import { initAuthentication, getAccessToken, isAuthenticated } from './authentication';

describe('authentication', () => {
  TestUtil.suspendLogger();

  beforeEach(() => {
    initState();
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('initAuthentication', () => {
    it('should return the authentication config and save it to state', () => {
      const authority = 'https://not-used.com/authorize';
      const clientId = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
      const redirectUri = 'https://not-used/authentication';
      const responseType = 'id_token token';
      const scope =
        'openid email profile read:consents write:consents read:data-holders read:use-cases write:tokens write:authorizations';

      const actualConfig = initAuthentication();

      expect(actualConfig.settings.authority).toEqual(authority);
      expect(actualConfig.settings.client_id).toEqual(clientId);
      expect(actualConfig.settings.redirect_uri).toEqual(redirectUri);
      expect(actualConfig.settings.response_type).toEqual(responseType);
      expect(actualConfig.settings.scope).toEqual(scope);
    });
  });

  describe('isAuthenticated', () => {
    it('should return the correct is authenticated state', async () => {
      const invalidExpiresAt = Math.floor(Date.now() / 1000);
      const validExpiresAt = invalidExpiresAt + 100;

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue(null);
      expect(isAuthenticated()).toBeFalsy();

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue('{INVALID_JSON');
      expect(isAuthenticated()).toBeFalsy();

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue('');
      expect(isAuthenticated()).toBeFalsy();

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue('{"some_prop":""}');
      expect(isAuthenticated()).toBeFalsy();

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue('{"access_token":""}');
      expect(isAuthenticated()).toBeFalsy();

      jest.spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem').mockReturnValue('{"access_token":"1234"}');
      expect(isAuthenticated()).toBeTruthy();

      jest
        .spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem')
        .mockReturnValue(`{"access_token":"1234", "expires_at":"${invalidExpiresAt}"}`);
      expect(isAuthenticated()).toBeFalsy();

      jest
        .spyOn(Object.getPrototypeOf(window.sessionStorage), 'getItem')
        .mockReturnValue(`{"access_token":"1234", "expires_at":"${validExpiresAt}"}`);
      expect(isAuthenticated()).toBeTruthy();
    });
  });

  describe('getAccessToken', () => {
    it('should return a blank invalid access token when the user has not authenticated', async () => {
      const expectedAccessToken = '';
      initAuthentication();

      const actualAccessToken = await getAccessToken();

      expect(actualAccessToken).toEqual(expectedAccessToken);
    });
  });
});
