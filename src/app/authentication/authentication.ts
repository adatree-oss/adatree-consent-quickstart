import { AuthContextProps, User, UserManager } from 'oidc-react';
import { getState, setState, StateType } from '../state/state';
import { APP_OIDC } from '../consts/oidc.const';
import { Logger } from '@adatree-oss/atomic-components';
import { APP_SETTINGS } from '../consts/settings.const';

export const initAuthentication = (): UserManager => {
  const userManager = new UserManager({
    authority: APP_OIDC.authority,
    client_id: APP_OIDC.clientId,
    extraQueryParams: JSON.parse(APP_OIDC.extraQueryParams),
    loadUserInfo: APP_OIDC.loadUserInfo,
    metadata: {
      authorization_endpoint: APP_OIDC.authority,
      end_session_endpoint: APP_OIDC.endSessionEndpoint,
      issuer: APP_OIDC.issuer,
      jwks_uri: APP_OIDC.jwksUri,
      token_endpoint: APP_OIDC.tokenEndpoint,
      userinfo_endpoint: APP_OIDC.userinfoEndpoint,
    },
    redirect_uri: APP_OIDC.redirectUri,
    response_type: APP_OIDC.responseType,
    scope: APP_OIDC.scope,
  });

  setState(StateType.Authentication, userManager);

  return userManager;
};

export const isAuthenticated = (): boolean => {
  let auth = false;
  if (APP_SETTINGS.simulateAuthentication) {
    return true;
  }

  const item = sessionStorage.getItem(`oidc.user:${APP_OIDC.authority}:${APP_OIDC.clientId}`);

  if (item) {
    try {
      auth = isAuthValid(item);
    } catch (error) {
      Logger.error('Error parsing authentication data from session storage', error);
    }
  }

  return auth;
};

export const getAccessToken = async (): Promise<string> => {
  return getState()
    .authentication.getUser()
    .then((user: User | null) => {
      return user ? user.access_token : '';
    });
};

const isAuthValid = (item: string): boolean => {
  const data = JSON.parse(item);
  if (data.expires_at) {
    return data && data.access_token && data.expires_at && isValidExpiresAt(data.expires_at);
  } else {
    return data && data.access_token;
  }
};

const isValidExpiresAt = (secondsSinceEpochTime: number): boolean => {
  const nowInSeconds = Math.floor(Date.now() / 1000);
  return nowInSeconds < secondsSinceEpochTime ? true : false;
};

export const signout = async (): Promise<void> => {
  const state = getState();

  if (state && state.authentication) {
    state.authentication.removeUser();
    state.authentication.signoutRedirect();
    return;
  }
};

export const getEmail = (auth: AuthContextProps): string => {
  let email = '';

  if (auth && auth.userData && auth.userData.profile) {
    if (auth.userData.profile.email !== undefined) {
      email = auth.userData.profile.email;
    } else if (auth.userData.profile.emails !== undefined) {
      email = auth.userData.profile.emails[0];
    }
  }

  return email;
};

export const getMobileNumber = (auth: AuthContextProps): string => {
  let mobileNumber = '';

  if (auth && auth.userData && auth.userData.profile) {
    if (auth.userData.profile.phone_number !== undefined) {
      mobileNumber = auth.userData.profile.phone_number;
    }
  }

  return mobileNumber;
};
