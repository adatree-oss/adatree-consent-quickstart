type Oidc = {
  authority: string;
  clientId: string;
  endSessionEndpoint: string;
  extraQueryParams: string;
  issuer: string;
  jwksUri: string;
  loadUserInfo: boolean;
  redirectUri: string;
  responseType: string;
  scope: string;
  tokenEndpoint: string;
  userinfoEndpoint: string;
};

export const APP_OIDC: Oidc = {
  authority: process.env.REACT_APP_OIDC_AUTHORITY_URI,
  clientId: process.env.REACT_APP_OIDC_CLIENT_ID,
  endSessionEndpoint: process.env.REACT_APP_OIDC_END_SESSION_ENDPOINT,
  extraQueryParams: process.env.REACT_APP_OIDC_EXTRA_QUERY_PARAMS, // JSON string example '{"audience":"https://some-domain.com/api/v2/"}'
  issuer: process.env.REACT_APP_OIDC_ISSUER,
  jwksUri: process.env.REACT_APP_OIDC_JWKS_URI,
  loadUserInfo: false,
  redirectUri: process.env.REACT_APP_OIDC_REDIRECT_URI,
  responseType: process.env.REACT_APP_OIDC_RESPONSE_TYPE,
  scope: process.env.REACT_APP_OIDC_SCOPE,
  tokenEndpoint: process.env.REACT_APP_OIDC_TOKEN_ENDPOINT,
  userinfoEndpoint: process.env.REACT_APP_OIDC_USERINFO_ENDPOINT,
};
