export type UrlItem = {
  label: string;
  url: string;
};

export const APP_URL = {
  AUTH_CALLBACK: { label: 'Redirect', url: '/redirect' },
  CONSENT_LIST: { label: 'Create and manage consent', url: '/consent' },
  CONSENT_CREATE: { label: 'Create consent', url: '/consent/create' },
  CONSENT_DETAIL: { label: 'Consent', url: '/consent/:consentId' },
  HOME: { label: 'Home', url: '/' },
  LOGOUT: { label: 'Logout', url: '/logout' },
  EXTERNAL_DOCS: { label: 'Documentation', url: 'https://developer.adatree.com.au/' },
  EXTERNAL_DESIGN_DOCS: { label: 'Design documentation', url: 'https://design.adatree.com.au/' },
};
