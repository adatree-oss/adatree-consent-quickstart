import { ConsentFormProvider } from '@adatree-oss/atomic-components';
import { ReactChild } from 'react';
import { PageWrapper } from '../../components/atoms/page-wrapper.atom';
import { Home } from '../../components/organisms/home/home-organism';
import { APP_URL } from '../consts/url.const';
import { AuthCallback } from '../../components/organisms/auth-callback/auth-callback.organism';
import { ConsentWizard } from '../../components/organisms/consent-wizard/consent-wizard.organism';
import { ConsentList } from '../../components/organisms/consent-list/consent-list.organism';
import { ConsentDetails } from '../../components/organisms/consent-details/consent-details.organism';
import { Button } from '@mui/material';
import { Logout } from '../../components/organisms/logout/logout.organism';

export type PageMeta = {
  path: string;
  component: ReactChild;
  isPrivate: boolean;
};

export type PageDocMeta = {
  path: string;
  mdxComponent: ReactChild;
};

const GeneralPages: PageMeta[] = [
  {
    path: APP_URL.HOME.url,
    component: (
      <PageWrapper>
        <Home />
      </PageWrapper>
    ),
    isPrivate: false,
  },
  {
    path: APP_URL.LOGOUT.url,
    component: (
      <PageWrapper>
        <Logout />
      </PageWrapper>
    ),
    isPrivate: false,
  },
];

const ConsentPages: PageMeta[] = [
  {
    path: APP_URL.AUTH_CALLBACK.url,
    component: (
      <PageWrapper>
        <AuthCallback />
      </PageWrapper>
    ),
    isPrivate: true,
  },
  {
    path: APP_URL.CONSENT_CREATE.url,
    component: (
      <PageWrapper width="md" breadcrumbs={[APP_URL.HOME, APP_URL.CONSENT_LIST, APP_URL.CONSENT_CREATE]}>
        <ConsentFormProvider>
          <ConsentWizard />
        </ConsentFormProvider>
      </PageWrapper>
    ),
    isPrivate: true,
  },
  {
    path: APP_URL.CONSENT_DETAIL.url,
    component: (
      <PageWrapper width="md" breadcrumbs={[APP_URL.HOME, APP_URL.CONSENT_LIST, APP_URL.CONSENT_DETAIL]}>
        <ConsentDetails />
      </PageWrapper>
    ),
    isPrivate: true,
  },
  {
    path: APP_URL.CONSENT_LIST.url,
    component: (
      <PageWrapper
        width="md"
        breadcrumbs={[APP_URL.HOME, APP_URL.CONSENT_LIST]}
        pageAction={
          <Button href={APP_URL.CONSENT_CREATE.url} variant="contained" color="secondary">
            New consent
          </Button>
        }
      >
        <ConsentList />
      </PageWrapper>
    ),
    isPrivate: true,
  },
];

export const Pages: PageMeta[] = [...GeneralPages, ...ConsentPages];
