import React from 'react';
import { Pages, PageMeta } from './pages';
import { Route, Switch } from 'react-router-dom';
import { PrivateRoute } from './private-route';

type RenderPageProps = {
  page: PageMeta;
};

const RenderPage = (props: RenderPageProps) => {
  const { page } = props;

  return (
    <>
      {page.isPrivate && <PrivateRoute>{page.component}</PrivateRoute>}
      {!page.isPrivate && <>{page.component}</>}
    </>
  );
};

export const Routes = () => {
  return (
    <Switch>
      {Pages.map((page: PageMeta) => (
        <Route exact path={page.path} key={page.path}>
          <RenderPage page={page} />
        </Route>
      ))}
    </Switch>
  );
};
