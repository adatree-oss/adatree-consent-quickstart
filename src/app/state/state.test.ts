import { initState, getState, setState, StateType } from './state';
import { UserManager } from 'oidc-react';

describe('State', () => {
  describe('initState', () => {
    it('should create a new instance of state', () => {
      const expectedState = { authentication: null };
      const actualState = initState();

      expect(actualState).toEqual(expectedState);
    });
  });

  describe('setState', () => {
    it('should set the authentication state', () => {
      const userManager = new UserManager({});
      const expectedState = { authentication: userManager };

      initState();
      setState(StateType.Authentication, userManager);

      const actualState = getState();

      expect(actualState).toEqual(expectedState);
    });
  });
});
