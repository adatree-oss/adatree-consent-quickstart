import { UserManager } from 'oidc-react';

/* Simple state system for the app, can always upgrade to Redux or similar if required */

declare global {
  interface Window {
    adt: State;
  }
}

export type State = {
  authentication: UserManager | null;
};

export enum StateType {
  Authentication = 'authentication',
}

export const initState = (): State => {
  window.adt = {
    authentication: null,
  };
  return window.adt;
};

export const getState = (): State => {
  return window.adt;
};

export const setState = (key: StateType, value: unknown): void => {
  if (key === 'authentication') {
    window.adt.authentication = value as UserManager;
  }
};
