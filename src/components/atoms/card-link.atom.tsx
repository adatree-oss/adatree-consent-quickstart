import { Box, Card, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

interface CardLinkProps {
  url: string;
  icon: React.ReactNode;
  title: string;
  content: string;
}

export const CardLink = (props: CardLinkProps) => {
  const { url, icon, title, content } = props;
  const externalLink = url.startsWith('http');
  const body = (
    <>
      <Box sx={{ pt: 4, textAlign: 'center', height: '120px' }}>{icon}</Box>
      <Box sx={{ py: 4, px: 2 }}>
        <Typography gutterBottom variant="h5" color="text.primary">
          {title}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {content}
        </Typography>
      </Box>
    </>
  );

  return (
    <Card sx={{ maxWidth: { md: 345 }, '&:hover': { backgroundColor: 'highlight.light' } }}>
      {externalLink && (
        <a href={url} style={{ display: 'block', height: '100%' }}>
          {body}
        </a>
      )}
      {!externalLink && (
        <Link to={url} style={{ display: 'block', height: '100%' }}>
          {body}
        </Link>
      )}
    </Card>
  );
};
