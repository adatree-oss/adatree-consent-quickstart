import React from 'react';
import { IconButton } from '@mui/material';
import LogoutIcon from '@mui/icons-material/Logout';

export type LogoutButtonProps = {
  handleClick: () => void;
};

export const LogoutButton: React.FC<LogoutButtonProps> = (props) => {
  const { handleClick } = props;

  return (
    <IconButton onClick={handleClick} sx={{ ml: 2, color: 'white', display: 'block', fontSize: '0' }}>
      <LogoutIcon />
    </IconButton>
  );
};
