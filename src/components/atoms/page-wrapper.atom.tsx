import { Container, Stack } from '@mui/material';
import { UrlItem } from '../../app/consts/url.const';
import { Breadcrumb } from './breadcrumb/breadcrumb.atom';

interface PageWrapperProps {
  children: React.ReactNode;
  breadcrumbs?: UrlItem[];
  pageAction?: React.ReactNode;
  width?: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
}

export const PageWrapper = (props: PageWrapperProps) => {
  const { children, breadcrumbs, pageAction, width = 'lg' } = props;

  return (
    <Container maxWidth={width} sx={{ mt: 6 }}>
      {breadcrumbs && (
        <Stack
          sx={{ mb: 8 }}
          direction={{ xs: 'column', sm: 'row' }}
          spacing={{ xs: 1, sm: 2, md: 4 }}
          alignItems="center"
          justifyContent="space-between"
          minHeight={'40px'}
        >
          <Breadcrumb breadcrumbs={breadcrumbs} />
          {pageAction}
        </Stack>
      )}
      {children}
    </Container>
  );
};
