import { Logger } from '@adatree-oss/atomic-components';
import { setApiInterceptors } from '../../../app/api/api';
import { APP_SETTINGS } from '../../../app/consts/settings.const';
import { initState } from '../../../app/state/state';

export const bootstrapApp = (): void => {
  Logger.info('App version', APP_SETTINGS.version, APP_SETTINGS.timestamp);

  setApiInterceptors();
  initState();
};
