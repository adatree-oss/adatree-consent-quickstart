import { useEffect, useState } from 'react';
import {
  ConsentDetails as Details,
  ConsentResponse,
  DataHolder,
  Logger,
  RevokeDialog,
} from '@adatree-oss/atomic-components';
import { Box, Skeleton } from '@mui/material';
import { useParams } from 'react-router-dom';
import consentRepository from '../../../app/api/repositories/consent-repository';

type ConsentDetailsParams = {
  consentId: string;
};

export const ConsentDetails = () => {
  const { consentId } = useParams<ConsentDetailsParams>();
  const [consent, setConsent] = useState<ConsentResponse>();
  const [dataHolders, setDataHolder] = useState<DataHolder[]>();
  const [dataHolderLogoUrl, setDataHolderLogoUrl] = useState<string>('');
  const [isLoading, setIsLoading] = useState(true);
  const [openConfirmationDialog, setOpenConfirmationDialog] = useState<boolean>(false);
  const [isProcessingRevoke, setIsProcessingRevoke] = useState<boolean>(false);

  useEffect(() => {
    consentRepository.findConsentById(consentId).then((foundConsent: ConsentResponse) => {
      setConsent(Object.assign({}, foundConsent));
      setIsLoading(false);
    });
  }, [consentId]);

  useEffect(() => {
    consentRepository.findAllDataHolders().then((dataHolders: DataHolder[]) => {
      setDataHolder(dataHolders);
    });
  }, []);

  useEffect(() => {
    if (dataHolders && consent) {
      dataHolders.forEach((dataHolder) => {
        if (dataHolder.dataHolderBrandId === consent.dataHolderBrandId) {
          setDataHolderLogoUrl(dataHolder.logoUri);
        }
      });
    }
  }, [consent, dataHolders]);

  const handleRevokeDialogClose = () => {
    setOpenConfirmationDialog(false);
  };

  const handleRevokeButtonClick = () => {
    setOpenConfirmationDialog(true);
  };

  const handleRevokeDialogButtonClick = () => {
    setIsProcessingRevoke(true);

    consentRepository
      .revokeConsent(consentId)
      .then((revokedConsent) => {
        setConsent(revokedConsent);
        setOpenConfirmationDialog(false);
        setIsProcessingRevoke(false);
      })
      .catch((error) => {
        Logger.error('Error handling Revoke', error);
        setOpenConfirmationDialog(false);
        setIsProcessingRevoke(false);
      });
  };

  return (
    <>
      {isLoading && (
        <>
          <Skeleton height={80} />
          <Skeleton height={80} />
          <Skeleton height={80} />
        </>
      )}
      {!isLoading && (
        <>
          <Box sx={{ mb: 4 }}>
            <Details
              consent={consent}
              dataHolderLogoUrl={dataHolderLogoUrl}
              dateTitle={'Key dates'}
              useCasetTitle={'Data we are currently receiving'}
              onRevokeClick={handleRevokeButtonClick}
            />
          </Box>

          <RevokeDialog
            isOpen={openConfirmationDialog}
            isLoading={isProcessingRevoke}
            dataHolderName={consent.dataHolderName}
            onCancelClick={handleRevokeDialogClose}
            onRevokeClick={handleRevokeDialogButtonClick}
          />
        </>
      )}
    </>
  );
};
