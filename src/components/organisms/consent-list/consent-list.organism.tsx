import { useEffect, useState } from 'react';
import { ConsentTabs, ConsentResponse, DataHolder } from '@adatree-oss/atomic-components';
import { Box } from '@mui/material';
import consentRepository from '../../../app/api/repositories/consent-repository';

export const ConsentList = () => {
  const [consents, setConsents] = useState<ConsentResponse[]>();
  const [dataHolders, setDataHolder] = useState<DataHolder[]>();
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    consentRepository.findAllConsents().then((consentResponses: ConsentResponse[]) => {
      setConsents(consentResponses);
      setIsLoading(false);
    });
  }, []);

  useEffect(() => {
    consentRepository.findAllDataHolders().then((dataHolders: DataHolder[]) => {
      setDataHolder(dataHolders);
    });
  }, []);

  return (
    <Box sx={{ mb: 4 }}>
      <ConsentTabs consents={consents} isLoading={isLoading} dataHolders={dataHolders} />
    </Box>
  );
};
