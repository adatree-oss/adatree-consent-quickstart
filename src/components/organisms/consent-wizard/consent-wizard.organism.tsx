import {
  ConsentStepperResponsive,
  CreateConsent,
  DataHolder,
  UseCaseResponse,
  DataAccessStep,
  InfoCdrStep,
  InfoHowItWorksStep,
  DataHolderStep,
  ReviewStep,
  useConsentForm,
  FeedbackMessage,
  ConsentResponse,
  Logger,
} from '@adatree-oss/atomic-components';
import { ReactNode, useEffect, useState } from 'react';
import { APP_SETTINGS } from '../../../app/consts/settings.const';
import { Box } from '@mui/material';
import { APP_URL } from '../../../app/consts/url.const';
import { useAuth } from 'oidc-react';
import { getEmail, getMobileNumber } from '../../../app/authentication/authentication';
import AlertCircle from 'mdi-material-ui/AlertCircle';
import Check from 'mdi-material-ui/Check';
import consentRepository from '../../../app/api/repositories/consent-repository';

export const ConsentWizard = () => {
  const auth = useAuth();
  const [consentForm] = useConsentForm();
  const [useCase, setUseCase] = useState<UseCaseResponse>();
  const [existingConsents, setExistingConsents] = useState<ConsentResponse[]>();
  const [dataHolders, setDataHolders] = useState<DataHolder[]>();
  const [feedback, setFeedback] = useState<ReactNode>();

  /**
   * Call the APIs
   */

  useEffect(() => {
    consentRepository
      .findAllUseCases()
      .then((foundUseCases: UseCaseResponse[]) => {
        const useCase = foundUseCases.filter((obj) => {
          return obj.id === APP_SETTINGS.consentUseCase;
        });
        if (!useCase[0]) {
          const error = `Error cannot find use case ${APP_SETTINGS.consentUseCase} in response from useCaseRepository.findAll()`;
          Logger.error(error, foundUseCases);
          throw new Error(error);
        }
        setUseCase(useCase[0]);
      })
      .catch((error) => {
        // This error needs to be handled
        Logger.error('Error calling useCaseRepository.findAll()');
        throw new Error(error);
      });
  }, []);

  useEffect(() => {
    consentRepository
      .findAllDataHolders()
      .then((dataHolders: DataHolder[]) => {
        setDataHolders(dataHolders);
      })
      .catch((error) => {
        // This error needs to be handled
        Logger.error('Error calling dataHolderRepository.findAll()');
        throw new Error(error);
      });
  }, []);

  useEffect(() => {
    consentRepository
      .findAllConsents()
      .then((foundConsents: ConsentResponse[]) => {
        setExistingConsents(foundConsents);
      })
      .catch((error) => {
        // This error needs to be handled
        Logger.error('Error calling consentRepository.findAll()');
        throw new Error(error);
      });
  }, []);

  /**
   * Submit the form
   */

  const handleSubmitForm = () => {
    const consent: CreateConsent = {
      consumerEmail: getEmail(auth),
      consumerMobileNumber: getMobileNumber(auth),
      dataHolderBrandId: consentForm.dataHolder.dataHolderBrandId,
      directMarketingAllowed: false,
      postUsageAction: consentForm.postUsageAction,
      sharingEndDate: consentForm.sharingEndDate.toISOString(),
      useCaseId: useCase.id,
    };

    const createdConsent = async () => {
      try {
        renderFeedback('Saving your consent request...', true);
        const createdConsent = await consentRepository.createConsent(consent);
        renderFeedback(`Please wait while we redirect you to ${consentForm.dataHolder.brandName}`);
        const redirect = await consentRepository.authorization(
          createdConsent.dataHolderBrandId,
          createdConsent.consentId
        );

        window.location.href = redirect;
      } catch (error) {
        renderFeedback('Sorry we were not able to process your request. Please try again later.', false, true);
      }
    };

    if (APP_SETTINGS.simulateBackend) {
      alert('You are simulating the backend. Cannot create a new Consent at this time');
    } else {
      createdConsent();
    }
  };

  /**
   * Configure the steps
   */

  // Each index in the array corresponds to a step
  const [disableNextButtons, setDisableNextButtons] = useState([false, true, true, false]);

  const steps = [
    {
      label: 'How it works',
      content: (
        <>
          <InfoHowItWorksStep />
          <Box sx={{ mt: 4 }}>
            <InfoCdrStep
              companyName={APP_SETTINGS.companyName}
              accreditationNumber={APP_SETTINGS.companyAccreditationNumber}
            />
          </Box>
        </>
      ),
      disableNextButton: disableNextButtons[0],
    },
    {
      label: 'Data',
      content: (
        <DataAccessStep
          companyName={APP_SETTINGS.companyName}
          useCase={useCase}
          isValid={(isValid) => {
            disableNextButtons[1] = !isValid;
            setDisableNextButtons([...disableNextButtons]);
          }}
        />
      ),
      disableNextButton: disableNextButtons[1],
    },
    {
      label: 'Connect',
      content: (
        <DataHolderStep
          dataHolders={dataHolders}
          isValid={(isValid) => {
            disableNextButtons[2] = !isValid;
            setDisableNextButtons([...disableNextButtons]);
          }}
          existingConsents={existingConsents}
          useCase={useCase}
          consentUrl={APP_URL.CONSENT_LIST.url}
        />
      ),
      disableNextButton: disableNextButtons[2],
    },
    {
      label: 'Summary',
      content: (
        <ReviewStep
          useCase={useCase}
          cdrPolicyUrl={APP_SETTINGS.companyCdrPolicyUrl}
          dataSharingRevocationEmail={APP_SETTINGS.companyDataSharingRevocationEmail}
        />
      ),
      nextButtonLabel: 'Consent',
      disableNextButton: disableNextButtons[3],
      onNext: handleSubmitForm,
    },
  ];

  const renderFeedback = (message: string, isLoading = false, isError = false) => {
    const icon =
      isError === true ? (
        <AlertCircle sx={{ fontSize: '56px', color: 'error.main' }} />
      ) : (
        <Check sx={{ fontSize: '56px', color: 'secondary.main' }} />
      );

    setFeedback(<FeedbackMessage message={message} icon={icon} showSpinner={isLoading} />);
  };

  return (
    <Box sx={{ mb: 4 }}>
      {feedback && feedback}
      {!feedback && <ConsentStepperResponsive steps={steps} />}
    </Box>
  );
};
