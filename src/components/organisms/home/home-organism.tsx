import { Container, Stack } from '@mui/material';
import { APP_URL } from '../../../app/consts/url.const';
import { CardLink } from '../../atoms/card-link.atom';

export const Home = () => {
  return (
    <Container maxWidth="xl">
      <Stack
        justifyContent="center"
        direction={{ xs: 'column', sm: 'column', md: 'row' }}
        spacing={{ xs: 4, sm: 6, md: 8 }}
        sx={{
          mt: { xs: 4, md: 8, lg: 16 },
          alignItems: { xs: 'center', md: 'stretch' },
          '& > div': { width: { xs: '100%', sm: '70%', lg: '33%' } },
        }}
      >
        <CardLink
          url={APP_URL.CONSENT_LIST.url}
          icon={<img src={'/images/phone.png'} alt="Adatree's CDR consent management page" />}
          title={APP_URL.CONSENT_LIST.label}
          content="Consent and connect your bank to the Adatree ADR Platform to see the process in action and access your data."
        />

        <CardLink
          url={APP_URL.EXTERNAL_DOCS.url}
          icon={<img src={'/images/apis.png'} alt="Adatree's Development documentation" />}
          title={APP_URL.EXTERNAL_DOCS.label}
          content="Explore our Dev and API documentation to integrate with the Adatree ADR Platform."
        />

        <CardLink
          url={APP_URL.EXTERNAL_DESIGN_DOCS.url}
          icon={<img src={'/images/quickstart.png'} alt="Adatree's Design documentation" />}
          title={APP_URL.EXTERNAL_DESIGN_DOCS.label}
          content="Explore our UI documentation to integrate with the Adatree ADR Platform."
        />
      </Stack>
    </Container>
  );
};
