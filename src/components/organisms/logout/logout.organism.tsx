import { FeedbackMessage } from '@adatree-oss/atomic-components';
import { Link, Typography } from '@mui/material';
import Check from 'mdi-material-ui/Check';
import { APP_URL } from '../../../app/consts/url.const';

export const Logout = () => {
  return (
    <FeedbackMessage
      message="You have been successfully logged out."
      icon={<Check sx={{ fontSize: '56px', color: 'secondary.main' }} />}
    >
      <Typography>
        Click <Link href={APP_URL.HOME.url}>here</Link> to return to the homepage.
      </Typography>
    </FeedbackMessage>
  );
};
