import * as React from 'react';
import { Link } from 'react-router-dom';
import { Alert, AppBar, Box, Container, Toolbar } from '@mui/material';
import { APP_URL } from '../../../app/consts/url.const';
import { APP_UI } from '../../../app/consts/ui.const';
import { APP_SETTINGS } from '../../../app/consts/settings.const';
import { LogoutButton } from '../../atoms/logout-button/logout-button.atom';
import { useAuth } from 'oidc-react';
import { isAuthenticated } from '../../../app/authentication/authentication';

export const Header = () => {
  const auth = useAuth();

  const handleLogout = () => {
    if (auth) {
      auth.userManager.removeUser();
      auth.userManager.signoutRedirect();
    }
  };

  let simulatedItems = '';
  simulatedItems = APP_SETTINGS.simulateAuthentication ? simulatedItems + ' authentication,' : simulatedItems;
  simulatedItems = APP_SETTINGS.simulateBackend ? simulatedItems + ' backend' : simulatedItems;

  return (
    <>
      <AppBar position="fixed" color="secondary">
        <Container maxWidth="xl">
          <Toolbar sx={{ borderBottom: '1px solid', borderColor: 'secondary.light' }} disableGutters>
            <Box sx={{ mr: 2, display: 'flex' }}>
              <Link to={APP_URL.HOME.url} style={{ lineHeight: 0 }}>
                <img alt="Adatree logo" src="/images/adatree.png" style={{ height: '36px' }} />
              </Link>
            </Box>

            <Box
              sx={{
                flexGrow: 0,
                flex: 'auto ',
                display: 'flex',
                justifyContent: 'end',
              }}
            >
              {simulatedItems && (
                <Alert severity="info" sx={{ px: 1, py: 0.2 }}>
                  Running with simulated {simulatedItems}
                </Alert>
              )}
              {isAuthenticated() && <LogoutButton handleClick={handleLogout} />}
            </Box>
          </Toolbar>
        </Container>
      </AppBar>

      {/* Empty Toolbar to push content down below the fixed Toolbar above */}

      <Box sx={{ minHeight: `${APP_UI.headerHeight}px` }} />
    </>
  );
};
