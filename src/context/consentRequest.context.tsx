import * as React from 'react';
import { CreateConsent } from '@adatree-oss/atomic-components';

const ConsentContext = React.createContext<[CreateConsent, React.Dispatch<React.SetStateAction<CreateConsent>>]>(null);

const useConsent = () => {
  const context = React.useContext(ConsentContext);
  if (!context) {
    throw new Error(`useConsent must be used within a ConsentProvider`);
  }
  return context;
};

const ConsentProvider = (props) => {
  const [consent, setConsent] = React.useState<CreateConsent>({
    consumerMobileNumber: undefined,
    sharingEndDate: undefined,
    dataHolderBrandId: undefined,
    useCaseId: undefined,
    postUsageAction: undefined,
    directMarketingAllowed: false,
  });
  const value = React.useMemo(() => [consent, setConsent], [consent]);
  return <ConsentContext.Provider value={value} {...props} />;
};

export { ConsentProvider, useConsent };
