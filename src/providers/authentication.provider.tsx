import React, { ReactNode } from 'react';
import { AuthProvider } from 'oidc-react';
import { useHistory } from 'react-router-dom';
import { initAuthentication } from '../app/authentication/authentication';
import { APP_URL } from '../app/consts/url.const';

interface Props {
  children: ReactNode;
}

export const AuthenticationProvider: React.FC<Props> = ({ children }: Props) => {
  const history = useHistory();
  const authConfig = initAuthentication();

  const oidcConfig = {
    userManager: authConfig,
    autoSignIn: false,
    onSignIn: async () => {
      history.push(APP_URL.CONSENT_LIST.url);
    },
  };

  return <AuthProvider {...oidcConfig}>{children}</AuthProvider>;
};
