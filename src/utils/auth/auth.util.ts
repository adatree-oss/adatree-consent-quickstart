import { Logger } from '@adatree-oss/atomic-components';

export const getAccessToken = (): string => {
  const accessToken = sessionStorage.getItem('ACCESS_TOKEN');
  return accessToken ? accessToken : '';
};

export const setAccessToken = (accessToken: string) => {
  sessionStorage.setItem('ACCESS_TOKEN', accessToken);
};

export const validateUriAndState = (state: string): boolean => {
  const STATE_KEY = 'client_state';
  const localState = sessionStorage.getItem(STATE_KEY);
  const currentUri = window.location.origin + window.location.pathname;
  let localRedirectUri;

  if (localState) {
    localRedirectUri = sessionStorage.getItem(localState);
  }

  if (localState === state && localRedirectUri === currentUri) {
    sessionStorage.removeItem(STATE_KEY);
    sessionStorage.removeItem(localState);
    return true;
  } else {
    Logger.error(
      `CSRF check failed. Local state ${localState} and local URI ${localRedirectUri} do not match callback state ${state} and callback URI ${currentUri}`
    );
    return false;
  }
};
